import 'dart:ffi';
import 'package:flutter/material.dart';

class UserProfile {
  String userId;
  String userName;
  String userTitle;
  String userUrl;
  String userIdlain;
  String resultcode;

  UserProfile({
    this.userId = "",
    this.userName = "",
    this.userTitle = "",
    this.userUrl = "",
    this.userIdlain = "",
    this.resultcode = "",
  });

  factory UserProfile.fromJson(Map<String, dynamic> json) {
    return UserProfile(
      userId: json["kode_dokter"] ?? '-',
      userName: json["nama_pegawai"] ?? '-',
      userTitle: json["nama_spesialisasi"] ?? '-',
      userUrl: json["url_foto_karyawan"] ?? '-',
      userIdlain: json["id_dd_user"] ?? '-',
      resultcode: json["code"] ?? '-',
    );
  }

  static List<UserProfile> fromJsonList(List list) {
    return list.map((item) => UserProfile.fromJson(item)).toList();
  }
}
