import 'dart:ffi';
import 'package:flutter/material.dart';

class AntrianPasienProfile {
  String no_kunjungan;
  String no_registrasi;
  String kode_bagian;
  String kode_bagian_tujuan;
  String kode_poli;
  bool flag_bayar;
  String id_pl_tc_poli;
  String no_antrian;
  String no_mr;
  String nama_px;
  String gender_px;
  String url_foto_px;
  String umur_px;
  String gol_darah_px;
  String alergi_px;
  String jam_daftar;
  String resultcode;
  String dokterid;

  AntrianPasienProfile({
    this.no_kunjungan = "",
    this.no_registrasi = "",
    this.kode_bagian = "",
    this.kode_bagian_tujuan = "",
    this.kode_poli = "",
    this.flag_bayar = false,
    this.id_pl_tc_poli = "",
    this.no_antrian = "",
    this.no_mr = "",
    this.nama_px = "",
    this.gender_px = "",
    this.url_foto_px = "",
    this.umur_px = "",
    this.gol_darah_px = "",
    this.alergi_px = "",
    this.jam_daftar = "",
    this.resultcode = "",
    this.dokterid = "",
  });

  factory AntrianPasienProfile.fromJson(Map<String, dynamic> json) {
    return AntrianPasienProfile(
      no_kunjungan: json["no_kunjungan"] ?? '-',
      no_registrasi: json["no_registrasi"] ?? '-',
      kode_bagian: json["kode_bagian"] ?? '-',
      kode_bagian_tujuan: json["kode_bagian_tujuan"] ?? '-',
      kode_poli: json["kode_poli"] ?? '-',
      flag_bayar: json["flag_bayar"] ?? false,
      id_pl_tc_poli: json["id_pl_tc_poli"] ?? '-',
      no_antrian: json["no_antrian"] ?? '-',
      no_mr: json["no_mr"] ?? '-',
      nama_px: json["nama_px"] ?? '-',
      gender_px: json["gender_px"] ?? '-',
      url_foto_px: json["url_foto_px"] ?? '-',
      umur_px: json["umur_px"] ?? '-',
      gol_darah_px: json["gol_darah_px"] ?? '-',
      alergi_px: json["alergi_px"] ?? '-',
      jam_daftar: json["jam_daftar"] ?? '-',
      resultcode: json["resultcode"] ?? '-',
    );
  }

  factory AntrianPasienProfile.fromJsonMR(Map<String, dynamic> json) {
    return AntrianPasienProfile(
      jam_daftar: json["tgl_periksa"] ?? '-',
      kode_bagian_tujuan: json["nama_bagian"] ?? '-',
      no_kunjungan: json["no_kunjungan"] ?? '-',
      nama_px: json["nama_dokter"] ?? '-',
    );
  }

  static List<AntrianPasienProfile> fromJsonList(List list) {
    return list.map((item) => AntrianPasienProfile.fromJson(item)).toList();
  }
}
