import 'dart:ffi';
import 'package:flutter/material.dart';

class PeriksaPasienProfile {
  String keadaan_umum;
  String kesadaran_pasien;
  String tekanan_darah;
  String nadi;
  String suhu;
  String pernafasan;
  String berat_badan;
  String tinggi_badan;
  String lingkar_kepala;
  String lingkar_dada;
  String skala_nyeri_face_number;
  String heart_rate;
  String lingkar_perut;
  String id_tc_status_pasien;
  String id_tc_soap;

  String subjektif;
  String objektif;
  String assesment;
  String code;

  PeriksaPasienProfile(
      {this.keadaan_umum = "",
      this.kesadaran_pasien = "",
      this.tekanan_darah = "",
      this.nadi = "",
      this.suhu = "",
      this.pernafasan = "",
      this.berat_badan = "",
      this.lingkar_kepala = "",
      this.lingkar_dada = "",
      this.tinggi_badan = "",
      this.skala_nyeri_face_number = "",
      this.subjektif = "",
      this.objektif = "",
      this.assesment = "",
      this.id_tc_status_pasien = "",
      this.id_tc_soap = "",
      this.code = "",
      this.heart_rate = "",
      this.lingkar_perut = ""});

  factory PeriksaPasienProfile.fromJson(Map<String, dynamic> json) {
    return PeriksaPasienProfile(
      keadaan_umum: json["keadaan_umum"] ?? '-',
      kesadaran_pasien: json["kesadaran_pasien"] ?? '-',
      tekanan_darah: json["tekanan_darah"] ?? '-',
      nadi: json["nadi"] ?? '-',
      suhu: json["suhu"] ?? '-',
      pernafasan: json["pernafasan"] ?? false,
      berat_badan: json["berat_badan"] ?? '-',
      lingkar_kepala: json["lingkar_kepala"] ?? '-',
      lingkar_dada: json["lingkar_dada"] ?? '-',
      tinggi_badan: json["tinggi_badan"] ?? '-',
      skala_nyeri_face_number: json["skala_nyeri_face_number"] ?? '-',
      id_tc_status_pasien: json["id_tc_status_pasien"] ?? '-',
      id_tc_soap: json["id_tc_soap"] ?? '-',
      heart_rate: json["heart_rate"] ?? '-',
      lingkar_perut: json["lingkar_perut"] ?? '-',
    );
  }

  static List<PeriksaPasienProfile> fromJsonList(List list) {
    return list.map((item) => PeriksaPasienProfile.fromJson(item)).toList();
  }
}
