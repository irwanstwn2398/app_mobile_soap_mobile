import 'dart:developer';

//import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:doctorapp/apiservice/serviceapi.dart';
import 'package:doctorapp/pages/pasien/component/list_antrian_pasiendetail.dart';
import 'package:doctorapp/pages/pasien/component/medrecord_pasien_det.dart';
import 'package:doctorapp/pages/pemeriksaan.dart';
import 'package:doctorapp/utils/colors.dart';
import 'package:doctorapp/utils/constant.dart';
import 'package:doctorapp/utils/strings.dart';
import 'package:doctorapp/utils/utility.dart';
import 'package:doctorapp/widgets/custom_card.dart';
import 'package:doctorapp/widgets/my_colors.dart';
import 'package:doctorapp/widgets/my_font_size.dart';
import 'package:doctorapp/widgets/mynetworkimg.dart';
import 'package:doctorapp/widgets/mysvgassetsimg.dart';
import 'package:doctorapp/widgets/mytext.dart';
import 'package:doctorapp/widgets/mytextformfield.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:doctorapp/model/periksamodel.dart';
import 'package:doctorapp/model/pasienmodel.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:doctorapp/pages/pasien/component/vitalsign_entry.dart';
import 'package:doctorapp/pages/pasien/component/soap_entry.dart';
import 'package:provider/provider.dart';
import 'package:doctorapp/provider/pasien_prov.dart';
import 'package:doctorapp/pages/pasien/component/diagnosis_entry.dart';

class MedicalRecPasienList extends StatefulWidget {
  final AntrianPasienProfile detailpasienProfile;
  const MedicalRecPasienList({Key? key, required this.detailpasienProfile})
      : super(key: key);

  @override
  State<MedicalRecPasienList> createState() => _MedicalRecPasienListState();
}

class _MedicalRecPasienListState extends State<MedicalRecPasienList> {
  PeriksaPasienProfile vitalSignPasien = new PeriksaPasienProfile();

  int selectedPos = 0;
  double bottomNavBarHeight = 60;
  final controller = ScrollController();

  var _scrollControllertrans = ScrollController();
  String _hasBeenPressed = '';
  List<AntrianPasienProfile> AntrianPasienList = [];
  List<AntrianPasienProfile> MRPasienList = [];

  @override
  void initState() {
    _getFirtsInfo();
    super.initState();
  }

  Widget bottomNav() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.fromLTRB(16, 10, 16, 0),
            child: StaggeredGrid.count(
              crossAxisCount: 3,
              mainAxisSpacing: 20,
              crossAxisSpacing: 20,
              children: [
                CustomCard(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => PrescriptionF()));
                  },
                  shadow: false,
                  width: double.infinity,
                  bgColor: MyColors.white,
                  borderRadius: BorderRadius.circular(15),
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomCard(
                        shadow: false,
                        height: 50,
                        width: 50,
                        bgColor: blue,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Isi Tindakan",
                        style: TextStyle(
                            color: MyColors.blackText,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                CustomCard(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => ListingAntrianPasien()));
                  },
                  shadow: false,
                  width: double.infinity,
                  bgColor: MyColors.white,
                  borderRadius: BorderRadius.circular(15),
                  padding: EdgeInsets.all(18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomCard(
                        shadow: false,
                        height: 50,
                        width: 50,
                        bgColor: blue,
                        borderRadius: BorderRadius.circular(100),
                        // child: Center(
                        //     child: Icon(
                        //   MyFlutterApp.group_96,
                        //   color: MyColors.white,
                        // ))
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Isi Resep",
                        style: TextStyle(
                            color: MyColors.blackText,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                CustomCard(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => ListingAntrianPasien()));
                  },
                  shadow: false,
                  width: double.infinity,
                  bgColor: MyColors.white,
                  borderRadius: BorderRadius.circular(15),
                  padding: EdgeInsets.all(18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomCard(
                        shadow: false,
                        height: 50,
                        width: 50,
                        bgColor: blue,
                        borderRadius: BorderRadius.circular(100),
                        // child: Center(
                        //     child: Icon(
                        //   MyFlutterApp.group_96,
                        //   color: MyColors.white,
                        // ))
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Isi ICD-10",
                        style: TextStyle(
                            color: MyColors.blackText,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ],
    );
  }

  Future<void> _getFirtsInfo() async {
    //return FirtsInfoPasienList;

    await UserApiService.getPasienMRPasienList(
            '', widget.detailpasienProfile.no_mr)
        .then((Vitaldata) {
      setState(() {
        MRPasienList = Vitaldata;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: white, // blue,
        //appBar: myAppBar(),
        body: Container(
            width: size.width,
            child: Stack(
              alignment: Alignment.topLeft,
              children: [
                profile(),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 110),
                    child: Container(
                      child: Padding(
                        //padding: const EdgeInsets.all(5.0),
                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 20),
                        child: Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              //Expanded(child: ItemSearch()),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ), //groupCategoryIsi

                Container(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 300),
                        child: Container(
                            child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: ListIsiRiwayat(),
                        ))))
              ],
            )),
      ),
    );
  }

  Future<void> ProfileDetail() async {}

  Widget profile() {
    // final AuthPasienData tablesProvider =
    //     Provider.of<AuthPasienData>(context, listen: false);
    // AntrianPasienProfile _modelprofile = tablesProvider.DataPeriksaProfile;

    return FutureBuilder(
        future: ProfileDetail(),
        builder: (context, _) => Padding(
            // padding: EdgeInsets.symmetric(horizontal: 0),
            padding: EdgeInsets.only(top: 0),
            // child: Positioned(
            //     top: 10,
            //     left: 30,
            child: Container(
              decoration: BoxDecoration(
                color: blue,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
              ),
              width: displayWidth(context),
              child: LayoutBuilder(builder: (context, constraints) {
                var parentHeight = constraints.maxHeight;
                var parentWidth = constraints.maxWidth;
                return SingleChildScrollView(
                    child: Column(children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      height: displayHeight(context) * .33, // 125,
                      width: displayWidth(context),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                width: 38,
                                child: ElevatedButton.icon(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  icon: Center(
                                    child: Icon(
                                      Icons.arrow_back,
                                      color: Color.fromARGB(255, 250, 247, 247),
                                      size: 25,
                                    ),
                                  ),
                                  label: Text(""),
                                  style: ButtonStyle(
                                    //minimumSize: new Size(30, 20),
                                    backgroundColor:
                                        MaterialStateProperty.all(blue),
                                    padding: MaterialStateProperty.all(
                                        EdgeInsets.all(2)),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: MyText(
                                        mTitle: 'Riwayat Pasien',
                                        mFontSize: 20,
                                        mFontStyle: FontStyle.normal,
                                        mFontWeight: FontWeight.normal,
                                        mTextAlign: TextAlign.center,
                                        mTextColor: white,
                                      ),
                                    )),
                              ),
                            ],
                          ),
                          CardProfile(vitalSignPasien)
                        ],
                      ),
                    ),
                  ),
                ]));
              }),
            )));
  }

  // AppBar myAppBar() {
  //   return AppBar(
  //     elevation: 0,
  //     centerTitle: true,
  //     backgroundColor: otherColor, //statusBarColor,
  //     leading: IconButton(
  //       onPressed: () {
  //         Navigator.of(context).pop();
  //       },
  //       icon: MySvgAssetsImg(
  //         imageName: "back.svg",
  //         fit: BoxFit.contain,
  //         imgHeight: Constant.backBtnHeight,
  //         imgWidth: Constant.backBtnWidth,
  //       ),
  //     ),
  //     title: MyText(
  //       mTitle: 'List Medical Record',
  //       mFontSize: 20,
  //       mFontStyle: FontStyle.normal,
  //       mFontWeight: FontWeight.normal,
  //       mTextAlign: TextAlign.center,
  //       mTextColor: white,
  //     ),
  //   );
  // }

  @override
  Widget CardProfile(PeriksaPasienProfile item) {
    final String NoKontrak;
    final GestureTapCallback? onTap;

    String urlfoto = widget.detailpasienProfile.url_foto_px;
    urlfoto = urlfoto.replaceAll('..', '');
    print(urlfoto);

    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
          height: MediaQuery.of(context).size.height * 0.23, // 125,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            //decoration for the outer wrapper
            color: white,
            borderRadius: BorderRadius.circular(22),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5), //color of shadow
                spreadRadius: 5, //spread radius
                blurRadius: 7, // blur radius
                offset: Offset(0, 2), // changes position of shadow
              ),
              //you can set more BoxShadow() here
            ],
          ),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(30),
              child: Container(
                  child: Stack(children: <Widget>[
                Positioned(
                  child: Container(
                      height: 310,
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                      flex: 8,
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [IsiCardProfileL(item)])),
                                  Expanded(
                                      flex: 2,
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(40),
                                              child: FadeInImage.assetNetwork(
                                                placeholder:
                                                    'assets/images/avatar.png',
                                                image:
                                                    urlfoto, //infoIsi.url_foto_px,
                                                fit: BoxFit.fill,
                                                width: 80,
                                                height: 90,
                                                imageErrorBuilder:
                                                    (context, url, error) =>
                                                        Icon(Icons.error),
                                              ),
                                            ),
                                            //dropdownMenu(),
                                            //Container(child: dropdownMenu())
                                          ])),
                                ],
                              )
                            ]),
                      )),
                )
              ])))),
    );
  }

  Widget IsiCardProfileL(PeriksaPasienProfile item) {
    return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(widget.detailpasienProfile.nama_px,
                style: GoogleFonts.poppins(
                    fontSize: 16, fontWeight: FontWeight.w800)),
            new SizedBox(
              height: 4,
              child: new Center(
                child: new Container(
                  margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
                  height: 2.0,
                  color: const Color.fromARGB(255, 241, 187, 183),
                ),
              ),
            ),
            SizedBox(height: 5),
            IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  //signSatu(context)
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _textWidgeColumn(
                            title: "No MR .",
                            value: widget.detailpasienProfile.no_mr),
                        SizedBox(height: 10),
                        _textWidgeColumn(
                            title: "Umur .",
                            value: widget.detailpasienProfile.umur_px),
                        SizedBox(height: 10),
                        _textWidgeColumn(
                            title: "Golongan Darah .",
                            value: widget.detailpasienProfile.gol_darah_px),
                        SizedBox(height: 10),
                        _textWidgeColumn(
                            title: "Jenis Kelamin .",
                            value: widget.detailpasienProfile.gender_px),
                        SizedBox(height: 10),
                        _textWidgeColumn(
                            title: "Alergi .",
                            value: widget.detailpasienProfile.alergi_px),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                ],
              ),
            ),
          ]),
        ));
  }

  @override
  Widget ButtonTindakan() {
    final String NoKontrak;
    final GestureTapCallback? onTap;

    PeriksaPasienProfile vitalsignDataDef;
    final AuthPasienData tablesProvider =
        Provider.of<AuthPasienData>(context, listen: false);
    vitalsignDataDef = tablesProvider.DataPeriksa;
    print(vitalsignDataDef.nadi);

    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
          height: MediaQuery.of(context).size.height * 0.24, // 125,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            //decoration for the outer wrapper
            color: Color.fromARGB(255, 250, 249, 248),
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 253, 250, 250)
                    .withOpacity(0.5), //color of shadow
                spreadRadius: 2, //spread radius
                blurRadius: 2, // blur radius
                offset: Offset(0, 2), // changes position of shadow
              ),
              //you can set more BoxShadow() here
            ],
          ),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                  child: Stack(
                children: <Widget>[
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 82,
                      minWidth: MediaQuery.of(context).size.width,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 3,
                        color: white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Container(
                          padding: const EdgeInsets.only(top: 20),
                          child: Padding(
                              padding: const EdgeInsets.all(15),
                              child: Container(
                                  //height: double.infinity,
                                  child: bottomNav())),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    transform: Matrix4.translationValues(20, -4, 0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4.0),
                      clipBehavior: Clip.antiAlias,
                      child: MyText(
                        mTitle: 'Planning Tindakan',
                        mFontSize: 18,
                        mOverflow: TextOverflow.ellipsis,
                        mMaxLine: 1,
                        mFontWeight: FontWeight.normal,
                        mTextAlign: TextAlign.start,
                        mTextColor: textTitleColor,
                      ),
                    ),
                  ),
                ],
              )))),
    );
  }

  Widget _textWidgeT({String? title, String? value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(title ?? '-',
              style:
                  //  TextStyle(
                  //     color: Colors.black,
                  //     fontWeight: FontWeight.bold,
                  //     fontSize: 16.sp),
                  TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontFamily: 'RobotoMono',
              )),
        ),
        Expanded(
          child: Text(
            value ?? '-',
            textAlign: TextAlign.end,
            style: TextStyle(
              fontSize: 14,
              color: Colors.black,
              fontFamily: 'RobotoMono',
            ),
          ),
        )
      ],
    );
  }

  Widget _textWidgeColumn({String? title, String? value}) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(title ?? '-',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: blue,
                  fontFamily: 'RobotoMono',
                )),
          ),
          Flexible(
            child: new Text(value ?? '-',
                textAlign: TextAlign.start,
                //overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontFamily: 'RobotoMono',
                )),
          )
        ],

        //),

        // Expanded(
        //   child: Text(
        //     value ?? '-',
        //     textAlign: TextAlign.end,
        //     style: TextStyle(
        //       fontSize: 14,
        //       color: Colors.black,
        //       fontFamily: 'RobotoMono',
        //     ),
        //   ),
        // )
      ),
    );
  }

  @override
  Widget ListIsiRiwayat() {
    return FutureBuilder<List<AntrianPasienProfile>>(
        //future: _getFirtsInfo,
        builder: (context, _) => Padding(
            padding: EdgeInsets.only(
              top: 10, //ScreenUtil().setHeight(ScreenUtil().setHeight(10)),
              left: 10,
            ), //ScreenUtil().setHeight(ScreenUtil().setWidth(10.0))),
            child: Container(
              height: double.infinity * 0.7,
              width: double.infinity, //  displayWidth(context),
              child: LayoutBuilder(builder: (context, constraints) {
                var parentHeight = constraints.maxHeight;
                var parentWidth = constraints.maxWidth;
                final List<AntrianPasienProfile>? items =
                    MRPasienList; // listSearchIsi;
                //AntrianPasienList;
                return items!.length == 0
                    ? Center(child: CircularProgressIndicator())
                    //   child: Column(children: [
                    //   Padding(
                    //     padding: const EdgeInsets.only(top: 0.0),
                    //     child: Container(
                    //       height: 120,
                    //       width: 120,
                    //       decoration: BoxDecoration(
                    //           image: DecorationImage(
                    //               image: AssetImage(
                    //                   'assets/images/avatar.png'))),

                    //     ),
                    //   ),
                    //   SizedBox(height: 20),
                    //   Text(
                    //     "Tidak Ada Riwayat Medical Record Pasien\nSaat Ini",
                    //     textAlign: TextAlign.center,
                    //     style: Utility.textTitleBlack,
                    //   ),
                    // ]))

                    : Scrollbar(
                        child: ListView.builder(
                          physics:
                              const AlwaysScrollableScrollPhysics(), //Even if zero elements to update scroll
                          itemCount: items.length,
                          scrollDirection: Axis.vertical,
                          controller: _scrollControllertrans,
                          itemBuilder: (context, index) {
                            return items[index] == null
                                ? CircularProgressIndicator()
                                : Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: items.length == 0
                                        ? Text('tak ada data')
                                        : IsiInfoRiwayatSearchandFilter(
                                            action: (() async {
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          MedicalRecPasienDetail(
                                                            detailpasienProfile:
                                                                items[index],
                                                          )));
                                            }),
                                            infoIsi: items[index],
                                            actionhover: () {
                                              MyTooltip(
                                                  child: Text('Oke'),
                                                  message: 'data');
                                            },
                                          ),
                                  );
                          },
                        ),
                      );
              }),
            )));
  }
}

class IsiInfoSearchandFilter extends StatelessWidget {
  final AntrianPasienProfile infoIsi;
  final Widget? prefWidget;
  final VoidCallback action;
  const IsiInfoSearchandFilter(
      {Key? key, required this.infoIsi, required this.action, this.prefWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String urlfoto = infoIsi.url_foto_px;
    urlfoto = urlfoto.replaceAll('..', '');

    return InkWell(
        onTap: () => action(),
        child: Container(
            height: 110,
            width: 390,
            child: IntrinsicHeight(
                child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0),
                    ),
                    shadowColor: Color.fromARGB(255, 198, 240, 11),
                    elevation: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Column(children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  flex: 2,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: MySvgAssetsImg(
                                            imageName: infoIsi
                                                        .kode_bagian_tujuan ==
                                                    "Laboratorium"
                                                ? "laboratorium.svg"
                                                : "doctor-checking-patient.svg",
                                            imgHeight: 90,
                                            imgWidth: 90,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ])),
                              SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                  flex: 8,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(infoIsi.kode_bagian_tujuan,
                                            style: GoogleFonts.poppins(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w800)),
                                        SizedBox(
                                          height: 4,
                                          child: Center(
                                            child: Container(
                                              margin:
                                                  const EdgeInsetsDirectional
                                                          .only(
                                                      start: 1.0, end: 1.0),
                                              height: 2.0,
                                              color: const Color.fromARGB(
                                                  255, 241, 187, 183),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        Text('Dokter. ' + infoIsi.nama_px,
                                            style: GoogleFonts.poppins(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600)),
                                        SizedBox(height: 2),
                                        Text(
                                            'Tanggal Periksa ' +
                                                infoIsi.jam_daftar,
                                            style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400)),
                                      ]))
                            ])
                      ]),
                    )))));
  }
}

class IsiInfoRiwayatSearchandFilter extends StatelessWidget {
  final AntrianPasienProfile infoIsi;
  final Widget? prefWidget;
  final VoidCallback action;
  final VoidCallback actionhover;
  //final FocusNode myFocusNode;
  const IsiInfoRiwayatSearchandFilter(
      {Key? key,
      required this.infoIsi,
      required this.action,
      required this.actionhover,
      //required this.myFocusNode,
      this.prefWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String urlfoto = infoIsi.url_foto_px;
    urlfoto = urlfoto.replaceAll('..', '');
    final splitted = infoIsi.jam_daftar.split(' ');

    return InkWell(
        onTap: () => action(),
        onHover: (val) => actionhover(),
        child: Container(
            //color: white,
            decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: gray),
              boxShadow: [
                BoxShadow(
                  color: gray.withOpacity(0.5), //color of shadow
                  spreadRadius: 2, //spread radius
                  blurRadius: 10, // blur radius
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
            ),
            height: 160,
            width: 146,
            child: IntrinsicHeight(
                child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Column(children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            //flex: 8,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Text('Nama Dokter .' + infoIsi.nama_px,
                                    style: GoogleFonts.nunito(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w800)),
                              ),
                              // new SizedBox(
                              //   height: 8,
                              //   child: new Center(
                              //     child: new Container(
                              //       margin: new EdgeInsetsDirectional.only(
                              //           start: 1.0, end: 1.0),
                              //       height: 2.0,
                              //       color: const Color.fromARGB(
                              //           255, 241, 187, 183),
                              //     ),
                              //   ),
                              // ),
                              SizedBox(height: 10),
                              Text(' Bagian . ' + infoIsi.kode_bagian_tujuan,
                                  style: GoogleFonts.nunito(
                                      fontSize: 16,
                                      color: blue,
                                      fontWeight: FontWeight.w800)),
                              SizedBox(height: 10),
                              Text(' Tanggal Pemeriksaan ',
                                  style: GoogleFonts.nunito(
                                      fontSize: 14,
                                      color: black,
                                      fontWeight: FontWeight.w800)),
                              SizedBox(height: 15),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              //Text(text),
                                              child: Icon(
                                                Icons.calendar_month,
                                                size: 20.0,
                                                color: blue,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(splitted[0],
                                                style: GoogleFonts.nunito(
                                                    fontSize: 14,
                                                    color: black,
                                                    fontWeight:
                                                        FontWeight.w800))
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 25,
                                      ),
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              //Text(text),
                                              child: Icon(
                                                Icons.watch_later_outlined,
                                                size: 20.0,
                                                color: blue,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(splitted[1],
                                                style: GoogleFonts.nunito(
                                                    fontSize: 14,
                                                    color: black,
                                                    fontWeight:
                                                        FontWeight.w800))
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              )
                            ]))
                      ]),
                ),
              ]),
            )))
        //)
        );
  }
}
