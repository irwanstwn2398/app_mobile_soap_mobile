import 'dart:developer';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:doctorapp/apiservice/serviceapi.dart';
import 'package:doctorapp/model/usermodel.dart';
import 'package:doctorapp/pages/pasien/component/list_antrian_pasiendetail.dart';
import 'package:doctorapp/pages/pemeriksaan.dart';
import 'package:doctorapp/provider/auth_user.dart';
import 'package:doctorapp/utils/colors.dart';
import 'package:doctorapp/utils/constant.dart';
import 'package:doctorapp/utils/strings.dart';
import 'package:doctorapp/utils/utility.dart';
import 'package:doctorapp/widgets/mynetworkimg.dart';
import 'package:doctorapp/widgets/mysvgassetsimg.dart';
import 'package:doctorapp/widgets/mytext.dart';
import 'package:doctorapp/widgets/mytextformfield.dart';
import 'package:flutter/material.dart';
import 'package:doctorapp/model/periksamodel.dart';
import 'package:doctorapp/model/pasienmodel.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:doctorapp/provider/pasien_prov.dart';
import 'package:provider/provider.dart';

class SOAPDokterUpdate extends StatefulWidget {
  const SOAPDokterUpdate({Key? key}) : super(key: key);

  @override
  State<SOAPDokterUpdate> createState() => _SOAPDokterUpdateState();
}

class _SOAPDokterUpdateState extends State<SOAPDokterUpdate> {
  PeriksaPasienProfile vitalsignData = new PeriksaPasienProfile();
  var _scrollControllerdet = ScrollController();
  final _formKey = GlobalKey<FormState>();

  TextEditingController signSubyektif = TextEditingController();
  TextEditingController signObyektif = TextEditingController();
  TextEditingController signAnamnese = TextEditingController();

  @override
  void initState() {
    _getUserProvider();
    super.initState();
  }

  _getUserProvider() async {
    final AuthPasienData tablesProvider =
        Provider.of<AuthPasienData>(context, listen: false);
    vitalsignData = tablesProvider.DataPeriksa;

    signSubyektif.text = vitalsignData.subjektif;
    signObyektif.text = vitalsignData.objektif;
    signAnamnese.text = vitalsignData.assesment;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: white,
        body: Stack(children: [
          Positioned(
            top: 5,
            right: 10,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 180.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Color.fromARGB(255, 238, 236, 236),
                    borderRadius: BorderRadius.circular(200),
                    border: Border.all(
                        width: 1, color: Color.fromARGB(255, 34, 33, 33))),
                height: 8.0,
                width: 50.0,
                //color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 22),
            child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: ListView(children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: RichText(
                        text: TextSpan(
                            text: 'Update ',
                            style: TextStyle(
                              fontSize: 25,
                              letterSpacing: 2,
                              color: Colors.yellow[700],
                            ),
                            children: [
                              TextSpan(
                                text: 'SOAP',
                                style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromARGB(255, 39, 28, 1),
                                ),
                              )
                            ]),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Subyektif',
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.w800)),
                        MyTextFormFieldEntry(
                          mController: signSubyektif, //mEmailController,
                          mObscureText: false,
                          mMaxLine: 10,
                          mHintTextColor: textHintColor,
                          mTextColor: otherColor,
                          mkeyboardType: TextInputType.text,
                          mTextInputAction: TextInputAction.next,
                          mWidth: 500,
                          mHeight: 200,
                          //mInputBorder: InputBorder.,
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Objektif',
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.w800)),
                        MyTextFormFieldEntry(
                          mController: signObyektif, //mEmailController,
                          mObscureText: false,
                          mMaxLine: 10,
                          mHintTextColor: textHintColor,
                          mTextColor: otherColor,
                          mkeyboardType: TextInputType.text,
                          mTextInputAction: TextInputAction.next,
                          mWidth: 500,
                          mHeight: 200,
                          //mInputBorder: InputBorder.,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Anamnese',
                                style: GoogleFonts.poppins(
                                    fontSize: 16, fontWeight: FontWeight.w800)),
                            MyTextFormFieldEntry(
                              mController: signAnamnese, //mEmailController,
                              mObscureText: false,
                              mMaxLine: 10,
                              mHintTextColor: textHintColor,
                              mTextColor: otherColor,
                              mkeyboardType: TextInputType.text,
                              mTextInputAction: TextInputAction.next,
                              mWidth: 500,
                              mHeight: 200,
                              //mInputBorder: InputBorder.,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                              child: SizedBox(
                            width: 100,
                            height: 40,
                            child: FittedBox(
                                child: FloatingActionButton.extended(
                              onPressed: () async {},
                              icon: const Icon(Icons.edit),
                              label: const Text('Submit'),
                              backgroundColor: Colors.redAccent,
                            )),
                          )),
                        ],
                      ),
                    )
                  ]),
                )),
          )
        ]));
  }

  Future updateData() async {
    final AuthPasienData tablesProvider =
        Provider.of<AuthPasienData>(context, listen: false);

    vitalsignData = tablesProvider.DataPeriksa;

    final AuthUserData dokterProvider =
        Provider.of<AuthUserData>(context, listen: false);
    UserProfile dokter = dokterProvider.DataUser;

    AntrianPasienProfile vitalsignDataProfile =
        tablesProvider.DataPeriksaProfile;

    vitalsignDataProfile.dokterid = dokter.userId;
    vitalsignData.subjektif = signSubyektif.text;
    vitalsignData.objektif = signObyektif.text;
    vitalsignData.assesment = signAnamnese.text;

    //update to APi
    //if success set to provider

    bool ok = await UserApiService.postvitalSOAPPasienAntri(
        vitalsignData, vitalsignDataProfile);

    tablesProvider.setDataPeriksa(vitalsignData);

    if (ok) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => DetailAntrianPasien(
                detailpasienProfile: vitalsignDataProfile,
              )));
    } else {
      await AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.SCALE,
        title: 'Data Tidak Valid',
        desc: 'cek data entry kembali',
        autoHide: const Duration(seconds: 5),
      ).show();
      return;
    }
  }
}
